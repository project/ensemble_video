<?php
/**
 * @file
 * Ensemble Video proxy callback include.
 */

/**
 * Page callback from ensemble_video_menu().
 */
function ensemble_video_proxy() {
  $api_url = !empty($_GET['request']) ? urldecode($_GET['request']) : '';
  $base_url = !empty($_GET['ensembleUrl']) ? urldecode($_GET['ensembleUrl']) : '';
  $cookie_prefix = str_replace('.', '_', $base_url);

  $whitelist = variable_get('ensemble_video_whitelist', '');
  if (!empty($whitelist)) {
    // Make sure our api url is in our whitelist.
    $domains = explode(',', $whitelist);
    $matched = FALSE;
    foreach ($domains as $domain) {
      $domain = trim($domain);
      if (preg_match('/^http(s)?:\/\/' . preg_quote($domain) . '(:[0-9]+)?/', $api_url) === 1) {
        $matched = TRUE;
        break;
      }
    }
    if ($matched) {
      if (isset($_COOKIE[$cookie_prefix . '-user']) && isset($_COOKIE[$cookie_prefix . '-pass'])) {
        // No need to urlencode the following since the drupal_http_request
        // moves these to the basic auth header.
        $username = $_COOKIE[$cookie_prefix . '-user'];
        $password = $_COOKIE[$cookie_prefix . '-pass'];
        $api_url = preg_replace('/http(s)?:\/\//', 'http$1://' . $username . ':' . $password . '@', $api_url);
      }
      $response = drupal_http_request($api_url);
      // Copied from proxy_router.
      if (!is_array($response)) {
        // Check for data.
        if (is_object($response) && !empty($response->data)) {
          // Set headers.
          if (is_array($response->headers)) {
            // Update content-length as it will have changed during
            // URL-rewrites.
            $response->headers['content-length'] = strlen($response->data);
            foreach ($response->headers as $header => $value) {
              // Strip basic auth header since we're using a form and cookies.
              if ($header != 'www-authenticate') {
                drupal_add_http_header($header, $value);
              }
            }
          }
          // Set status.
          drupal_add_http_header('Status', $response->code . " " . $response->status_message);
          // Print actual data.
          print $response->data;
          exit;
        }
        else {
          watchdog('ensemble_video', 'No data found in response.', array(), WATCHDOG_WARNING);
        }
      }
      else {
        // Watchdog.
        foreach ($response as $message) {
          watchdog('ensemble_video', $message, array(), WATCHDOG_WARNING);
        }
      }
    }
    else {
      watchdog('ensemble_video', 'API url domain not found in whitelist.', array(), WATCHDOG_WARNING);
    }
  }
  else {
    watchdog('ensemble_video', 'No white-listed domains configured.', array(), WATCHDOG_WARNING);
  }
  // FIXME - better response code for exceptional condition?
  drupal_not_found();
}
