<?php
/**
 * @file
 * Ensemble Video admin settings include.
 */

/**
 * Page callback: Ensemble Video settings.
 *
 * @see ensemble_video_menu()
 */
function ensemble_video_admin_settings($form, &$form_state) {
  $form['ensemble_video_whitelist'] = array(
    '#type' => 'textarea',
    '#title' => t('Ensemble Video domain whitelist'),
    '#default_value' => variable_get('ensemble_video_whitelist', ''),
    '#description' => t('A comma-delimited list of allowed Ensemble Video domains for proxying (e.g. "cloud.ensemblevideo.com, ensemble.myschool.edu").'),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}

/**
 * Make sure we have valid domains in our whitelist.
 */
function ensemble_video_admin_settings_validate($form, &$form_state) {
  $whitelist = $form_state['values']['ensemble_video_whitelist'];
  $domains = explode(',', $whitelist);
  // From http://stackoverflow.com/questions/106179/regular-expression-to-match-hostname-or-ip-address
  $valid_ip_address_regex = '/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/';
  $valid_hostname_regex = '/^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/';
  foreach ($domains as $domain) {
    $domain = trim($domain);
    if (!(preg_match($valid_hostname_regex, $domain) === 1 || preg_match($valid_ip_address_regex, $domain) === 1)) {
      form_set_error('ensemble_video_whitelist', t('"@domain" appears to be an invalid domain name.', array('@domain' => $domain)));
    }
  }
}
