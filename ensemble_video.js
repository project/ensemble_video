(function($) {

    var decorated = "ev-decorated", apps = [];

    var videoOptions = {
            fieldWrap: ".ensemble-video-fieldset",
            settingsClass: EV.VideoSettings,
            fieldClass: ".ensemble-video-embed-settings",
            embedClass: ".ensemble-embedded-video"
    };

    var playlistOptions = {
            fieldWrap: ".ensemble-playlist-fieldset",
            settingsClass: EV.PlaylistSettings,
            fieldClass: ".ensemble-playlist-embed-settings",
            embedClass: ".ensemble-embedded-playlist"
    };

    // We add the name of our field instance as a class to the element so we can find the matching Drupal settings
    var initField = function(element, settings, callback) {
        // Find settings that match one of our class names
        var instanceName = _.find($(element).attr("class").split(" "), function(clazz) {
            return Drupal.settings[clazz];
        });
        var fieldSettings = settings[instanceName];
        if (fieldSettings) {
            // We create a separate app for each field instance (since settings can differ between instances)
            if (!apps[instanceName]) {
                apps[instanceName] = new EV.EnsembleApp({
                    ensembleUrl: fieldSettings.base_url,
                    pageSize: fieldSettings.page_size,
                    authPath: settings.basePath,
                    urlCallback: function(url) {
                        return settings.basePath + "?q=ensemble&ensembleUrl=" + encodeURIComponent(fieldSettings.base_url) + "&request=" + encodeURIComponent(url);
                    }
                });
            }
            callback(apps[instanceName], fieldSettings);
        }
    };

    var getAttachHandler = function(options) {
        return function(context, settings) {
            // "Decorate" our form fields (initialize ensemble video/playlist pickers)
            $(options.fieldWrap).once(decorated, function() {
                initField(this, settings, _.bind(function(app) {
                    var jsonStr = $(options.fieldClass, this).val();
                    var jsonObj = (jsonStr ? JSON.parse(jsonStr) : {});
                    app.done(_.bind(function() {
                        app.handleField(this, new options.settingsClass(jsonObj), options.fieldClass);
                    }, this));
                }, this));
            });
            // "Decorate" our display fields
            $(options.embedClass).once(decorated, function() {
                initField(this, settings, _.bind(function(app, fieldSettings) {
                    app.done(_.bind(function() {
                        app.handleEmbed(this, new options.settingsClass(fieldSettings[this.id]));
                    }, this));
                }, this));
            });
        };
    };

    Drupal.behaviors.ensemble_video_video = {
            attach : getAttachHandler(videoOptions)
    };

    Drupal.behaviors.ensemble_video_playlist = {
            attach : getAttachHandler(playlistOptions)
    };

})(jQuery);
