
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Adding Field Instances to Content Types
 * Security Considerations
 * More Information


INTRODUCTION
------------

Current maintainer: Jim Pease <jmpease@ensemblevideo.com>

The Ensemble Video module for Drupal provides additional fields enabling content
editors to easily embed videos and/or playlists from Ensemble Video
installations.  Users with permission to manage Drupal's structure can add one
or more video and/or playlists field instances to any content type.  Each field
instance can be configured to search and embed media from any Ensemble Video
installation.

The goal of this module is to simplify the process of embedding Ensemble Video
content that would otherwise require navigation within Drupal and Ensemble Video
to copy and paste desired embed codes.


REQUIREMENTS
------------

PHP 5.3+

Uses:
- lodash.js 0.10.0
- backbone.js  0.9.2

This module includes the JavaScript library dependencies automatically; there
are no additional steps that you need to take.  However, the module's use of the
above libraries may cause problems with other modules that do the same using
different library versions.


INSTALLATION
------------

Follow the instructions in the Drupal contributed module installation guide:
http://drupal.org/documentation/install/modules-themes/modules-7


CONFIGURATION
-------------

Once installed and enabled, the module requires some minimal global
configuration before it can be successfully used.  Either click the 'Configure'
link in the 'Operations' section of the 'Ensemble Video' module or navigate to
Administration -> Configuration -> Ensemble Video to find the configuration
settings.

In the 'Ensemble Video domain whitelist' field, enter the domain names for any
Ensemble Video installations you would like to be accessible from your Drupal
site.  Do not use the protocol or path when entering the domain.  For example,
for the installation at 'https://cloud.ensemblevideo.com/ensemble' you would
enter 'cloud.ensemblevideo.com'.  Separate multiple domains with a comma.  When
complete, be sure to click 'Save configuration'.


ADDING FIELD INSTANCES TO CONTENT TYPES
---------------------------------------

Before you can begin to embed Ensemble Video content, you need to add the
appropriate fields to the desired content type.  Navigate to Administration ->
Structure -> Content Types and click 'manage fields' under 'Operations' for that
type.

Under 'Add new field' enter a label for your field, such as 'Ensemble Video'.
This is the label that will be displayed for this field when content of this
type is added or edited.  The automatically generated machine name can typically
be left as is, unless you have multiple fields with the same label. In that case
you would have to modify the machine name to make sure it is unique among field
instances.  Assuming the Ensemble Video module is installed and configured
correctly,  you should see additional 'Ensemble Embedded Video' and 'Ensemble
Embedded Playlist' options in the 'Field Type' drop-down list.  Select the
appropriate type and the corresponding widget list should be automatically
updated with the necessary widget (e.g. 'Ensemble Video Picker' for 'Ensemble
Embedded Video').  Next click 'Save' to add the field.

Once saved, you will be taken to a global 'Field Settings' screen.  Since there
is no necessary configuration for this step, simply click 'Save field settings'
to continue.

Next you are required to configure settings specific to this instance of the
field. Here you can modify the field label and provide some optional help text
to be displayed when adding or editing content containing this field.  In the
'Ensemble Video Base URL' field, you *must* enter the full url to the
application root of your Ensemble Video installation (for e.g.
'https://cloud.ensemblevideo.com/ensemble').  The required 'Ensemble Video Page
Size' controls how many results are retrieved at a time when searching videos
and playlists. This can typically be left at the default value, but can be
modified depending on the performance of your Ensemble Video installation and/or
the needs of your end-users.  If your Ensemble Video installation supports
authentication domains, you can optionally set the desired domain in the
'Ensemble Video Authentication Domain' field.

You have the option to choose a default value for the field and can select the
desired number of videos or playlists users can embed using this field.  Be sure
to click 'Save settings' once you are done entering desired values.

NOTE:  It is *important* to note that the default value can *not* be set when
*first* configuring the field.  This is due to a known issue...the picker
requires the value set in the 'Ensemble Video Base URL' field, but this value is
not available to the picker until the field instance settings are saved.  The
default value can, however, be chosen later by editing these settings after the
initial save.  Also note that the same issue applies when modifying the base url
setting.  Once changed the settings need to be saved, then edited, prior to
choosing a different default video/playlist.

For more information on the process outlined in this section, see the Drupal
guide 'Working with content types and fields':
http://drupal.org/documentation/modules/field-ui


SECURITY CONSIDERATIONS
-----------------------

The functionality provided by this module requires end-users to authenticate
against Ensemble Video installations in order to search available content.  This
authentication uses session cookies to temporarily store user credentials. While
not required, it is *strongly* recommended that SSL is enabled (and enforced)
for your Drupal site *and* Ensemble Video installation!  Without SSL,
credentials are passed in the clear, which may result in a compromise of your
Ensemble Video installation.


MORE INFORMATION
----------------

For more information regarding Ensemble Video, visit our website at
https://www.ensemblevideo.com/

For more information regarding this module, or to submit a feature request or
report a bug, go to
http://drupal.org/project/ensemble_video
